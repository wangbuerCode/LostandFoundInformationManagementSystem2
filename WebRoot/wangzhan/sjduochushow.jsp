<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="header.jsp"%>
<%@ include file="../jiazai/sousuobaohan.jsp"%>
<script type="text/javascript">
var userId = <%=userId%>;
	
	function addSjlaiyuan(sjduochuId){
		var sjlaiyuanName = document.form1.sjlaiyuanName.value;
		if(sjlaiyuanName==""){
			document.form1.sjlaiyuanName.focus();
			alert("请填写回复");
			return false;
		}
		if (userId==0) {
			alert("请登录后回复");
			return false;
		}
		$.post("<%=basePath %>addSjlaiyuan?sjlaiyuanType="+userId+"&sjlaiyuanName="+sjlaiyuanName,{sjlaiyuanType1:sjduochuId},function(result){
			if(result.errorMsg){
				alert(result.errorMsg);
			}else{
				location.reload();
				alert("回复成功！");
			}
		},"json");
	}
	
</script>
    
    <!-- 内容 -->
    <div class="container">
        <div class="row">

            <div class="col-xs-12 col-sm-8 col-md-9" id="rightBox">

                <div class="positionBox">
                    <div class="titleBar">
                        <h1>当前位置</h1>
                        <span></span>
                        <a href="<%=basePath %>">首页</a> > <a href="<%=basePath %>wangzhan/sjduochulist.jsp"><%=newJcpeizhi.getSjduochuBieming() %>信息</a>
                    </div>
                </div>

                <div class="col-sm-12 col-md-12 pad">
                    
                    <div class="detailTitle" style="border:0px; background:none; font-size:20px; color:#000;">
                        <%=sousuoSjduochu.getSjduochuName()%>
                    </div>

                    <div class="detailTime">
                       	 发布人：<%=sousuoSjduochu.getUserName()%>　发布时间：<%=DateUtil.formatDate(sousuoSjduochu.getSjduochuDate(),"yyyy-MM-dd HH:mm:ss") %>
                    </div>
                
                    <div class="detailContent">
                
                        <span style="color:#505050;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:14px;line-height:22px;">
                        &nbsp;&nbsp;&nbsp;&nbsp;<%=sousuoSjduochu.getSjduochuMark()%>
                        </span>

                    </div>
                    <div class="detailContent">
                	<table width="100%"  border="0" cellspacing="0" cellpadding="0" style="padding-top:15px;">
              	<h5>回复信息</h5>
              <%if(sousuoSjlaiyuans.size()!=0) {%>
				<%for(int i = 0; i < sousuoSjlaiyuans.size(); i++){ %>
					<%Sjlaiyuan newSjlaiyuan = sousuoSjlaiyuans.get(i); %>
              	<tr>
              		<td style="border:1px solid #e0e0e0; border-top:0px; padding:10px; line-height:25px;">
              			<p><b>楼层：</b><%=i+1 %>&nbsp;&nbsp;&nbsp;&nbsp;<b>用户：</b><%=newSjlaiyuan.getSjlaiyuanMark2() %>&nbsp;&nbsp;&nbsp;&nbsp;<b>时间：</b><%=DateUtil.formatDate(newSjlaiyuan.getSjlaiyuanDate(),"yyyy-MM-dd HH:mm:ss") %><br></p>
              			<p><b>回复：</b><%=newSjlaiyuan.getSjlaiyuanName() %><br></p>
              		</td>
              	</tr>
				<%} %>
			  <%} %>
              </table>
                    </div>
                    <div class="detailContent">
                <form name="form1" method="post">
             	<p><textarea rows="5" cols="80" name="sjlaiyuanName" id="sjlaiyuanName"></textarea></p>
				<p><a href="javascript:addSjlaiyuan(<%=sousuoSjduochu.getSjduochuId()%>)"><input type="button" value="提交回复" /></a></p>
			 </form>

                    </div>

                </div>

            </div>

            <%@ include file="dcleft.jsp"%>

        </div>
    </div>
<%@ include file="footer.jsp"%>